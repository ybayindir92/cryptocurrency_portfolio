# Cryptocurrency Portfolio 

Cryptocurrency Portfolio is a web application to track your cryptocurrency purchases.  
It collects current values of cryptocurrencies from bitfinex api.  
Application can be reached at [Heroku](https://portfolio-cryptocurrency.herokuapp.com/)

##### To start with locally...
'gradle bootFullApplication' command is available for running the application.  
This task installs the node.js, builds client and backend.  
The project contains two profiles 'standalone' and 'local'.    
Standalone is applied as a default profile.    
If you want to use your local postgresql database you need to configure your database credentials in application profiles. 

##### Technologies
* Angular 6
* Spring Boot
* Restful Api
* Spring Mvc
* Spring Data
* Postgresql
* H2
* Gradle