import {Component, OnInit} from '@angular/core';
import {PurchaseService} from "../service/purchase.service";
import {Purchase} from "../model/purchase.model";
import * as moment from 'moment'
import SweetAlert from 'sweetalert2'

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {
  APP_DATE_FORMAT = "DD.MM.YYYY";
  purchaseList: Purchase[] = [];
  purchase: Purchase = {
    id: null,
    cryptoCurrency: "BITCOIN",
    purchaseDate: moment(new Date()).format(this.APP_DATE_FORMAT),
    walletLocation: null,
    amount: 1,
    marketValue: 0
  };

  constructor(private purchaseService: PurchaseService) {
  }

  ngOnInit() {
    this.getAll();
  }

  savePurchase() {
    let validationErrors = this.validateForm(this.purchase);
    if (validationErrors.length > 0) {
      SweetAlert(validationErrors.join("\n"));
      return;
    }
    this.purchaseService.save(this.purchase)
      .subscribe(data => {
        this.purchaseList.push(data);
      });
  }

  deletePurchase(id) {
    SweetAlert({
      title: 'Are you sure?',
      text: 'You will not be able to recover this record!',
      type: 'warning',
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        this.purchaseService.delete(id)
          .subscribe(data => {
            this.purchaseList = this.purchaseList.filter(u => u.id !== id);
          });
      }
    });
  }

  getAll() {
    this.purchaseService.list()
      .subscribe(data => {
        if (data) {
          this.purchaseList = data;
        }
      });
  }

  private validateForm(purchase: Purchase): string [] {
    let validationErrors: string[] = [];
    if (!purchase.cryptoCurrency || !purchase.amount || !purchase.purchaseDate || !purchase.walletLocation) {
      validationErrors.push("Please enter all the fields! ");
    }
    if (!this.isDateValid(purchase.purchaseDate)) {
      validationErrors.push("Date must be valid and entered in " + this.APP_DATE_FORMAT);
    }
    if (purchase.amount < 1 || isNaN(purchase.amount)) {
      validationErrors.push("Please enter valid amount. It must be grater than zero!");
    }
    return validationErrors;
  }

  private isDateValid(date): boolean {
    return moment(date, this.APP_DATE_FORMAT, true).isValid();
  }

}
