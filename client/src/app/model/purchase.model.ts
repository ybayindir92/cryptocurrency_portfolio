export class Purchase {

  id: string;
  cryptoCurrency: string;
  amount: number;
  purchaseDate: string;
  walletLocation: string;
  marketValue: number;
}
