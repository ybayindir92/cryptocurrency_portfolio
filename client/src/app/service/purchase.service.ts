import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Purchase} from "../model/purchase.model";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class PurchaseService {

  constructor(private http: HttpClient) {
  }

  private userUrl = '/api/';

  public save(purchase) {
    return this.http.post<Purchase>(this.userUrl + 'purchase', purchase);
  }

  public list() {
    return this.http.get<Purchase[]>(this.userUrl + 'purchases');
  }

  delete(id) {
    return this.http.delete(this.userUrl + 'purchase/' + id);
  }
}
