package com.yasemin.cryptoportfolio.service;

import com.yasemin.cryptoportfolio.model.Cryptocurrency;
import com.yasemin.cryptoportfolio.model.SupportedCurrency;
import com.yasemin.cryptoportfolio.model.TickerResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TickerService {

    @Value("${bitfinex.api.ticker.url}")
    private String tickerUrl;

    private final RestTemplate restTemplate;

    private Optional<TickerResource> getCurrentTicker(Cryptocurrency cryptocurrency) {
        String symbol = cryptocurrency.getCode() + SupportedCurrency.EURO.getCode();
        String url = tickerUrl + symbol;
        try {
            return Optional.of(restTemplate.getForObject(url, TickerResource.class));
        } catch (RestClientException e) {
            log.warn("Ticker service exception: " + e);
            return Optional.empty();
        }
    }

    public double getCurrentValue(Cryptocurrency cryptocurrency) {
        final Optional<TickerResource> currentTicker = getCurrentTicker(cryptocurrency);
        return currentTicker.map(TickerResource::getLastPrice).orElse(0.0);
    }

}