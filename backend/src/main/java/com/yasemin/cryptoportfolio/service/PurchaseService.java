package com.yasemin.cryptoportfolio.service;

import com.yasemin.cryptoportfolio.model.Purchase;
import com.yasemin.cryptoportfolio.repository.PurchaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final TickerService tickerService;

    public List<Purchase> listAll() {
        final List<Purchase> purchaseList = purchaseRepository.findAll();
        purchaseList.forEach(purchase -> purchase.setMarketValue(calculateMarketValue(purchase)));
        return purchaseList;
    }

    public Purchase retrieve(Long purchaseId) {
        return purchaseRepository.findById(purchaseId).orElse(null);
    }

    public Purchase save(Purchase purchase) {
        purchaseRepository.save(purchase);
        purchase.setMarketValue(calculateMarketValue(purchase));
        return purchase;
    }

    public void delete(Long purchaseId) {
        purchaseRepository.deleteById(purchaseId);
    }

    private int calculateMarketValue(final Purchase purchase) {
        return (int) (purchase.getAmount() * tickerService.getCurrentValue(purchase.getCryptoCurrency()));
    }
}
