package com.yasemin.cryptoportfolio.controller;

import com.yasemin.cryptoportfolio.model.Purchase;
import com.yasemin.cryptoportfolio.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PurchaseController {

    private final PurchaseService purchaseService;

    @GetMapping(value = "/purchases")
    public ResponseEntity<List<Purchase>> listAllPurchases() {
        List<Purchase> purchases = purchaseService.listAll();
        if (purchases.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(purchases, HttpStatus.OK);
    }

    @GetMapping(value = "/purchase/{id}")
    public ResponseEntity<Purchase> getPurchase(@PathVariable("id") long id) {
        Purchase purchase = purchaseService.retrieve(id);
        if (purchase == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(purchase, HttpStatus.OK);
    }

    @PostMapping(value = "/purchase")
    public ResponseEntity<Purchase> createPurchase(@RequestBody @Valid Purchase purchase, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        purchaseService.save(purchase);
        return new ResponseEntity<>(purchase, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/purchase/{id}")
    public ResponseEntity deletePurchase(@PathVariable("id") long id) {
        Purchase purchase = purchaseService.retrieve(id);
        if (purchase == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        purchaseService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
