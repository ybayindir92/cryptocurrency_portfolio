package com.yasemin.cryptoportfolio.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ValidDateImpl implements ConstraintValidator<ValidDate, String> {

    @Override
    public void initialize(final ValidDate constraintAnnotation) {
    }

    @Override
    public boolean isValid(final String dateToValidate, final ConstraintValidatorContext context) {
        if (dateToValidate == null) {
            return false;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(dateToValidate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
