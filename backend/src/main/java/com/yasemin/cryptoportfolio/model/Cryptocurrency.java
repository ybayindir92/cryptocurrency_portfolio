package com.yasemin.cryptoportfolio.model;

import lombok.Getter;

@Getter
public enum Cryptocurrency {
    BITCOIN("btc"),
    ETHERIUM("eth"),
    RIPPLE("xrp");

    private String code;

    Cryptocurrency(final String code) {
        this.code = code;
    }

}
