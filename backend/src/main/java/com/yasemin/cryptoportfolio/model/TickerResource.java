package com.yasemin.cryptoportfolio.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TickerResource {
    private double mid;
    private double bid;
    private double ask;
    private double low;
    private double high;
    private double volume;
    private double timestamp;
    @JsonProperty("last_price")
    private double lastPrice;
}
