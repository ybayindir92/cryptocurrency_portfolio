package com.yasemin.cryptoportfolio.model;

import com.yasemin.cryptoportfolio.validation.ValidDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PURCHASE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CRYPTO_CURRENCY")
    @Enumerated(EnumType.STRING)
    @NotNull
    private Cryptocurrency cryptoCurrency;

    @Column(name = "AMOUNT")
    @Min(1)
    private int amount;

    @Column(name = "PURCHASE_DATE")
    @ValidDate
    private String purchaseDate;

    @Column(name = "WALLET_LOCATION")
    @NotEmpty
    private String walletLocation;

    @Transient
    private int marketValue;

}
