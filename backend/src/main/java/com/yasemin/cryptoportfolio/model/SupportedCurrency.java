package com.yasemin.cryptoportfolio.model;

import lombok.Getter;

@Getter
public enum SupportedCurrency {

    EURO("eur");

    private String code;

    SupportedCurrency(final String code) {
        this.code = code;
    }
}
