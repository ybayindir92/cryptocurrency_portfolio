package com.yasemin.cryptoportfolio.repository;

import com.yasemin.cryptoportfolio.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

}
