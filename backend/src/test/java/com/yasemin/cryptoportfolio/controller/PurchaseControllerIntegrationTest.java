package com.yasemin.cryptoportfolio.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasemin.cryptoportfolio.model.Cryptocurrency;
import com.yasemin.cryptoportfolio.model.Purchase;
import com.yasemin.cryptoportfolio.repository.PurchaseRepository;
import com.yasemin.cryptoportfolio.service.TickerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.yasemin.cryptoportfolio.model.Cryptocurrency.BITCOIN;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PurchaseControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PurchaseRepository purchaseRepository;

    @MockBean
    private TickerService tickerService;

    @Captor
    private ArgumentCaptor<Purchase> purchaseCaptor;

    @Captor
    private ArgumentCaptor<Long> longCaptor;

    @Test
    public void shouldFindAllPurchases() throws Exception {
        // given
        List<Purchase> mockPurchases = singletonList(anyPurchase("walletLocation_X"));
        given(purchaseRepository.findAll()).willReturn(mockPurchases);

        // when
        final MockHttpServletResponse response = mockMvc.perform(get("/api/purchases")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("walletLocation_X");
    }

    @Test
    public void shouldSavePurchase() throws Exception {
        // given
        final String purchaseJson = anyPurchaseJson("walletLocation1");

        // when
        final MockHttpServletResponse response = mockMvc.perform(post("/api/purchase")
                .contentType(MediaType.APPLICATION_JSON)
                .content(purchaseJson))
                .andReturn().getResponse();

        // then
        verify(purchaseRepository).save(purchaseCaptor.capture());
        assertThat(purchaseCaptor.getValue().getWalletLocation()).isEqualTo("walletLocation1");
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    public void shouldReturnNoContentWhenEmptyPurchaseList() throws Exception {
        // given
        given(purchaseRepository.findAll()).willReturn(emptyList());

        // when
        final MockHttpServletResponse response = mockMvc.perform(get("/api/purchases")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnNotFoundIfThereIsNoPurchase() throws Exception {
        // given
        given(purchaseRepository.findById(1L)).willReturn(Optional.empty());

        // when
        final MockHttpServletResponse response = mockMvc.perform(get("/api/purchase/1")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldRetrievePurchase() throws Exception {
        // given
        given(purchaseRepository.findById(1L)).willReturn(Optional.of(anyPurchase("walletLocation_X")));

        // when
        final MockHttpServletResponse response = mockMvc.perform(get("/api/purchase/1")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("walletLocation_X");
    }

    @Test
    public void shouldDeletePurchase() throws Exception {
        // given
        given(purchaseRepository.findById(1L)).willReturn(Optional.of(new Purchase()));

        // when
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/purchase/1")).andReturn().getResponse();

        // then
        verify(purchaseRepository).deleteById(longCaptor.capture());
        assertThat(longCaptor.getValue()).isEqualTo(1L);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void shouldReturnNotFoundIfThereIsNoPurchaseToDelete() throws Exception {
        // given
        given(purchaseRepository.findById(1L)).willReturn(Optional.empty());

        // when
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/purchase/1")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldNotSavePurchaseWithValidationErrors() throws Exception {
        // given
        final Purchase purchase = Purchase.builder().amount(-1).cryptoCurrency(BITCOIN).build();
        final String purchaseJson = getPurchaseJson(purchase);

        // when
        final MockHttpServletResponse response = mockMvc.perform(post("/api/purchase")
                .contentType(MediaType.APPLICATION_JSON)
                .content(purchaseJson))
                .andReturn().getResponse();

        // then
        verify(purchaseRepository, never()).save(any());
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    private Purchase anyPurchase(String walletLocation) {
        return Purchase.builder()
                .walletLocation(walletLocation)
                .cryptoCurrency(getRandomCryptoCurrency())
                .purchaseDate("01.01.2018")
                .amount(new Random().nextInt(10)).build();
    }

    private String getPurchaseJson(Purchase purchase) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(purchase);
    }

    private String anyPurchaseJson(String walletLocation) throws JsonProcessingException {
        return getPurchaseJson(anyPurchase(walletLocation));
    }

    private Cryptocurrency getRandomCryptoCurrency() {
        final Cryptocurrency[] cryptocurrencies = Cryptocurrency.values();
        int randomIndex = new Random().nextInt(cryptocurrencies.length);
        return cryptocurrencies[randomIndex];
    }

}