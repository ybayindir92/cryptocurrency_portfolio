package com.yasemin.cryptoportfolio.service;

import com.yasemin.cryptoportfolio.model.Purchase;
import com.yasemin.cryptoportfolio.repository.PurchaseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.yasemin.cryptoportfolio.model.Cryptocurrency.BITCOIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceUnitTest {

    @Mock
    private PurchaseRepository purchaseRepository;

    @Mock
    private TickerService tickerService;

    @InjectMocks
    private PurchaseService purchaseService;

    @Test
    public void shouldCallSaveMethodOfRepository() {
        // given
        final Purchase purchase = Purchase.builder().cryptoCurrency(BITCOIN).amount(10).build();
        given(tickerService.getCurrentValue(any())).willReturn(120.59);

        // when
        purchaseService.save(purchase);

        // then
        verify(purchaseRepository).save(purchase);
    }

    @Test
    public void shouldCallDeleteMethodOfRepository() {
        // given
        final Long id = 5L;

        // when
        purchaseService.delete(id);

        // then
        verify(purchaseRepository).deleteById(id);
    }

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        purchaseService.listAll();

        // then
        verify(purchaseRepository).findAll();
    }

    @Test
    public void shouldAddIntegerMarketValueToSavedPurchase() {
        // given
        final Purchase purchase = Purchase.builder().cryptoCurrency(BITCOIN).amount(10).build();
        given(tickerService.getCurrentValue(BITCOIN)).willReturn(120.59);

        // when
        final Purchase savedPurchase = purchaseService.save(purchase);

        // then
        assertThat(savedPurchase.getMarketValue()).isEqualTo(1205);
    }

    @Test
    public void shouldAddMarketValueToAllList() {
        // given
        final Purchase purchase1 = Purchase.builder().cryptoCurrency(BITCOIN).amount(2).build();
        final Purchase purchase2 = Purchase.builder().cryptoCurrency(BITCOIN).amount(3).build();
        given(purchaseRepository.findAll()).willReturn(Arrays.asList(purchase1, purchase2));
        given(tickerService.getCurrentValue(any())).willReturn(120.0);

        // when
        final List<Purchase> purchaseList = purchaseService.listAll();

        // then
        assertThat(purchaseList).extracting(Purchase::getMarketValue).containsExactly(240, 360);
    }
}
