package com.yasemin.cryptoportfolio.service;

import com.yasemin.cryptoportfolio.model.TickerResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static com.yasemin.cryptoportfolio.model.Cryptocurrency.BITCOIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TickerServiceIntegrationTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private TickerService tickerService;

    @Test
    public void shouldReturnCurrentValue() {
        // given
        TickerResource tickerResource = TickerResource.builder().lastPrice(123.5).build();
        given(restTemplate.getForObject(anyString(), any())).willReturn(tickerResource);

        // when
        final double actual = tickerService.getCurrentValue(BITCOIN);

        // then
        assertThat(actual).isEqualTo(123.5);
    }

    @Test
    public void shouldReturnEmptyWhenExceptionThrown() {
        // given
        given(restTemplate.getForObject(anyString(), any())).willThrow(new RestClientException("Rest exception"));

        // when
        final Double actual = tickerService.getCurrentValue(BITCOIN);

        // then
        assertThat(actual).isEqualTo(0.0);
    }

}