package com.yasemin.cryptoportfolio.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(JUnit4.class)
public class ValidDateImplUnitTest {

    public static final String DATE_FORMAT = "dd/MM/yyyy";

    private ValidDateImpl dateValidator;

    @Before
    public void init() {
        dateValidator = new ValidDateImpl();
    }

    @Test
    public void shouldReturnFalseWhenDateIsNull() {
        // given
        String date = null;

        // when
        final boolean isDateValid = dateValidator.isValid(date, null);

        // then
        assertThat(isDateValid).isFalse();
    }

    @Test
    public void shouldReturnFalseWhenDateInWrongFormat() {
        // given
        String date = "01/01/2018";

        // when
        final boolean isDateValid = dateValidator.isValid(date, null);

        // then
        assertThat(isDateValid).isFalse();
    }

    @Test
    public void shouldReturnTrueWhenDateInCorrectFormat() {
        // given
        String date = "01.01.2018";

        // when
        final boolean isDateValid = dateValidator.isValid(date, null);

        // then
        assertThat(isDateValid).isTrue();
    }

    @Test
    public void shouldReturnFalseWhenDateIsUnreal() {
        // given
        String date = "15.15.2018";

        // when
        final boolean isDateValid = dateValidator.isValid(date, null);

        // then
        assertThat(isDateValid).isFalse();
    }

}